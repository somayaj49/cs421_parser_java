
import java.util.Arrays;
import java.util.HashSet;

public class Main {
	public static boolean wordInGrammar(String g, String w) {
		Earley ep = new Earley();
		return ep.solve(g.split(";"), w);
	}

	public static void main(String[] args) {
		// TC1
		// System.out.println(wordInGrammar("S->T+T;T->1|2", "1+2"));
		// TC2
		// System.out.println(wordInGrammar("S->SxS;S->a;", "ab"));
		// TC3
		// System.out.println(wordInGrammar("S->a;", "a"));
		// TC4
		System.out.println(wordInGrammar("S->aB;B->b;B->~;", "ab"));
		// prints out a line with the derivatives in the list.
		// ex: derivates=[aB, ab]

	}
}