# parser_java

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/somayaj49/parser_java.git
git branch -M main
git push -uf origin main
```
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/somayaj49/parser_java/-/settings/integrations)

## Instructions for executing Main.java 
- Compile Main.java -> javac -d . Main.java
- Execute Main.java -> java Main.java

## Test cases and execution 

```
// TC1
// System.out.println(wordInGrammar("S->T+T;T->1|2", "1+2"));
// TC2
// System.out.println(wordInGrammar("S->SxS;S->a;", "ab"));
// TC3
// System.out.println(wordInGrammar("S->a;", "a"));
// TC4
System.out.println(wordInGrammar("S->aB;B->b;B->~;", "ab"));
// prints out a line with the derivatives in the list.
// ex: derivates=[aB, ab]
```
